import { s_email, s_fontSize, s_preview } from '../selectors'

describe('configuration', () => {

  beforeEach(() => {
    cy.visit('/')
  })

  it('prevents generation for invalid email', () => {

    cy.get(s_email)
      .should('be.visible')
      .type('wrong_example.com{enter}')
      .should($email => {
        expect($email).attr('aria-invalid', 'true')
      })

  })

  it('toggles more settings', () => {

    cy.get(s_fontSize)
      .should('not.be.visible')

    cy.get('label').contains('more settings')// TODO: merge this selector or figure out new one
      .should('be.visible')
      .click()

    cy.get(s_fontSize)
      .should('be.visible')

  })

  it('handles font size', () => {

    cy.get(s_fontSize)
      .should('not.be.visible')

    cy.get('label').contains('more settings')
      .click()

    cy.get(s_email)
      .type('foo@example.com')

    cy.get(s_fontSize)
      .type('10px{enter}')

    cy.get(s_preview)
      .should('be.visible')

  })

  it('validates font size', () => {

    cy.get('label').contains('more settings')
      .click()

    cy.get(s_email)
      .type('foo@example.com')

    cy.get(s_fontSize)
      .type('1000{enter}')
      .should($fontSize => {
        expect($fontSize).attr('aria-invalid', 'true')
      })

    cy.get(s_preview)
      .should('not.be.visible')

  })

})
