import { s_code, s_email, s_preview } from '../selectors'

describe('obfuscation', () => {
  beforeEach(() => {
    cy.visit('/')
  })
  it('generates obfuscated email', () => {
    cy.get(s_code)
      .should('not.be.visible')

    cy.get(s_email)
      .should('be.visible')
      .type('foo@example.com{enter}')

    cy.get(s_code)
      .should('be.visible')
      .and('not.have.html', '')

    cy.get(s_preview)
      .should('be.visible')
      .and('not.have.html', '')

  })
})
