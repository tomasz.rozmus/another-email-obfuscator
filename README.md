# Email obfuscator

Simple email obfuscator, a presentational open source project under development

Demo [here](http://stage.aeo.xnyl.ovh)

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn e2e`
Runs Cypress and performs end to end tests

### Other
App was based on [CRA](docs/CRA.md)

Some other topics have their own doc files
[in the docs folder](docs)
