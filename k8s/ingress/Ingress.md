#Ingress
Nginx ingress controller is required on the cluster to serve multiple domain applications.
https://kubernetes.github.io/ingress-nginx/deploy/
```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.30.0/deploy/static/mandatory.yaml
```

Replace IP with Cluster IP, and create a ingress service with:
```
kubectl apply -f ingress-service.yaml
```
