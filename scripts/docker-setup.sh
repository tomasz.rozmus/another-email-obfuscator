#!/usr/bin/env bash

# docker login asks for the password to be passed through stdin for security
# we use $CI_JOB_TOKEN here which is a special token provided by GitLab
echo -n $CI_JOB_TOKEN | docker login -u gitlab-ci-token --password-stdin $CI_REGISTRY
