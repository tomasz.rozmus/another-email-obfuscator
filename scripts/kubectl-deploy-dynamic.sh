#!/usr/bin/env bash

# requires KUBE_ENVIRONMENT, PACKAGE_TAG, KUBE_CLUSTER_IP, KUBE_DEPLOYMENT_DOMAIN
# optional AUTH_BASIC_HTPASSWD

echo "Attempting to deploy to $KUBE_DEPLOYMENT_DOMAIN"
env | grep PACKAGE_TAG

export CONFIG_FILE=k8s/configs/${KUBE_ENVIRONMENT}.env
[[ ! -f "$CONFIG_FILE" ]] && touch "$CONFIG_FILE"
[[ -n "$AUTH_BASIC_HTPASSWD" ]] && echo "HTPASSWD_FILE=bat:$AUTH_BASIC_HTPASSWD" >> "$CONFIG_FILE"

kubectl create configmap app-env-configmap --from-env-file="$CONFIG_FILE" -o yaml --dry-run | kubectl apply -f -
cat k8s/templates/deployment.template.yaml | sed "s/{{APP_IMAGE_TAG}}/$PACKAGE_TAG/g" | kubectl apply -f -
cat k8s/templates/service.template.yaml | sed "s/{{KUBE_CLUSTER_IP}}/$KUBE_CLUSTER_IP/g" | kubectl apply -f -
cat k8s/templates/ingress.template.yaml | sed "s/{{APP_DOMAIN}}/$KUBE_DEPLOYMENT_DOMAIN/g" | kubectl apply -f -
