#!/usr/bin/env bash

cat k8s/templates/environment.template.yaml | sed "s/{{NAMESPACE}}/$NAMESPACE/g" | kubectl apply -f -
kubectl -n $NAMESPACE describe secret $(kubectl -n $NAMESPACE get secret | grep admin | awk '{print $1}')
