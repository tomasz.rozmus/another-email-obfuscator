#!/usr/bin/env bash

# requires KUBE_CLUSTER_IP, KUBE_NAMESPACE, KUBE_ENVIRONMENT_ADMIN_TOKEN, KUBE_CLUSTER_NAME

echo Setting up kubectl for $KUBE_NAMESPACE

kubectl config set-cluster app-cluster --server https://${KUBE_CLUSTER_IP}:6443 --insecure-skip-tls-verify=true
kubectl config set-credentials ${KUBE_NAMESPACE}-admin --token=$KUBE_ENVIRONMENT_ADMIN_TOKEN
kubectl config set-context ${KUBE_NAMESPACE}-context
kubectl config use-context ${KUBE_NAMESPACE}-context
kubectl config set-context --current --cluster=app-cluster --user=${KUBE_NAMESPACE}-admin --namespace=$KUBE_NAMESPACE

kubectl config view
kubectl get all
