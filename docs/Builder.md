# Builder
This is a builder with typescript, ts-node, lerna, kubectl, helm and psql preinstalled. You can use it with gitlab-ci. Based on this article and gitlab docs
https://codeblog.dotsandbrackets.com/continuous-integration-deployment/

## modifying builder

Build with:  
```docker build -f builder/Dockerfile -t xnyl/app-builder:vX ./builder```  

Login to docker:
```docker login docker.io```    

Push image
```docker push xnyl/app-builder:vX```  

Test image

```docker run -it xnyl/app-builder:vX bash```
