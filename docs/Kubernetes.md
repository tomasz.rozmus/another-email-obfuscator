# Tamplates usage

## Before

Make sure you are using correct kubernetes cluster with `kubectl cluster-info` or ``


## For each new environment <ENV>
edit and execute `NAMESPACE=<YOUR_NAMESPACE> && . ./scripts/kubectl-create-namespace.sh`
save token `KUBE_<ENV>_ADMIN_TOKEN` in CI/CD vars

## Other
### List variables
```
cat k8s/templates/environment.template.yaml | grep -oE "\{{(.*?)\}}" | uniq
```

### Deploy Token
Generate deploy token in gitlab `gitlab-puller-1`, copy and save `<password>`, then 
```
kubectl create secret --namespace=$NAMESPACE docker-registry gitlab-registry --docker-server=registry.gitlab.com --docker-username=gitlab-puller-1 --docker-password=<password> --docker-email=tomasz.rozmus@gmail.com --output yaml | kubectl apply -n $NAMESPACE -f -
```
