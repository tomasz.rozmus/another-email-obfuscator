import React, { Suspense } from 'react'
import { LightHeader } from './components/wrappers/LightHeader'
import { Main } from './components/wrappers/Main'
import { Wrapper } from './components/wrappers/Wrapper'
import { HomePage } from './pages/HomePage'

// loading component for suspense fallback
const Loader = () => (
  <div className='GlobalLoader'>
    <img src='/public/logo512.png' alt='loader'/>
    <div>loading...</div>
  </div>
)

export function App () {
  return (
    <Suspense fallback={<Loader />}>
      <Wrapper>
        <Main>
          <LightHeader />
          <HomePage />
        </Main>
      </Wrapper>
    </Suspense>
  )
}

export default App
