import { combineReducers } from '@reduxjs/toolkit'
import { configurationReducer } from '../features/configuration/configurationSlice'
import { testingReducer } from '../features/testing/testingSlice'

export const rootReducer = combineReducers({
  configuration: configurationReducer,
  testing: testingReducer
})

export type RootState = ReturnType<typeof rootReducer>

