export const fontWeightBold = 700

export const fontSizeH2 = 32
export const fontSizeH3 = 28
export const fontSizeH4 = 28

export const descriptionVariant = 'body1'

export const smallMargin = 32
export const mediumMargin = 48
export const bigMargin = 72
export const veryBigMargin = 96

export const paperPaddingTop = 30
export const paperPaddingLeft = 44

// animationConstants
export const tooltipEntryDelay = 1333
