import { fontSizeH2, fontSizeH3, fontSizeH4, fontWeightBold } from './settings'

export const typography = {
  fontFamily: [
    '"Montserrat"',
    '"Roboto"',
    '"Helvetica"',
    '"Arial"',
    'sans-serif'
  ].join(','),
  fontWeightMedium: fontWeightBold,
  h1: {
    fontSize: 44
  },
  h2: {
    fontSize: fontSizeH2,
    fontWeight: fontWeightBold
  },
  h3: {
    fontSize: fontSizeH3,
    fontWeight: fontWeightBold
  },
  subtitle1: {
    fontSize: fontSizeH2,
    lineHeight: 1.22
  },
  subtitle2: {
    fontSize: fontSizeH4,
    fontWeight: 'normal' as any,
    lineHeight: 1.21
  }
}
