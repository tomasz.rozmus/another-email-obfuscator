export const props = {
  MuiTextField: {
    autoComplete: 'off',
    fullWidth: true,
    form: {
      autocomplete: 'off'
    }
  }
}
