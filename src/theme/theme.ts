import { createMuiTheme, responsiveFontSizes } from '@material-ui/core'
import { blue, teal } from '@material-ui/core/colors'
import { overrides } from './overrides'
import { props } from './props'
import { typography } from './typography'

let theme = createMuiTheme({
  props,
  overrides,
  typography,
  palette: {
    primary: teal,
    secondary: blue
  }
})

theme = responsiveFontSizes(theme)

export { theme }
