import { fontWeightBold } from './settings'

export const overrides = {
  MuiInputLabel: {
    shrink: {
      transform: 'translate(0, 1.5px) scale(0.8125);'
    }
  },
  MuiInputBase: {
    input: {
      fontWeight: fontWeightBold
    }
  },
  MuiButton: {
    root: {
      fontWeight: fontWeightBold
    }
  }
}
