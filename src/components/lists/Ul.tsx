import { createStyles, Theme, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import React, { FunctionComponent } from 'react'
import { smallMargin } from '../../theme/settings'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      marginBottom: smallMargin
    },
  }),
)

export const Ul: FunctionComponent = ({ ...props }) => {
  const styles = useStyles()
  return <Typography component='ul' className={styles.root} {...props} />
}
