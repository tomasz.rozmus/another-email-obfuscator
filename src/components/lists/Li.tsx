import { Typography } from '@material-ui/core'
import React, { FunctionComponent } from 'react'
import { descriptionVariant } from '../../theme/settings'


export const Li: FunctionComponent = ({ ...props }) => <Typography variant={descriptionVariant} component='li' {...props} />
