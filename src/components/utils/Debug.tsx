import React from 'react'
import { createStyles, makeStyles } from '@material-ui/core'

const useStyles = makeStyles(() => createStyles({
  root: {
    background: 'lightgray'
  }
}))

const off = process.env.NODE_ENV === 'production'
export const Debug = (props: any) => {
  const classes = useStyles()
  const target = props.target || props.children
  return off ? null : (<pre className={classes.root}>{JSON.stringify(target, null, 2)}</pre>)
}
