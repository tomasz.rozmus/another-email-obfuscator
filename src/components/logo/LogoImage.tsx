import { createStyles, Theme } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import AlternateEmailIcon from '@material-ui/icons/AlternateEmail'
import BlurOnIcon from '@material-ui/icons/BlurOn'
import classNames from 'classnames'
import React, { FunctionComponent } from 'react'

const animationTime = '0.9s'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'block',
      fontSize: '5em',
      width: '1.25em',
      height: '1.25em',
      position: 'relative'
    },
    icon: {
      position: 'absolute',
      top: 0,
      left: 0,
      transition: `${animationTime} filter linear`,
    },
    iconSharp: {
      filter: 'blur(0)',
      '&:hover': {
        filter: 'blur(0.025em)',
      }
    },
    iconBlur: {
      top: '25%',
      left: '25%',
      color: theme.palette.primary.dark,
      filter: 'blur(0.025em)',
      '&:hover': {
        filter: 'blur(0)',
      }
    },
  }),
)

export const LogoImage: FunctionComponent =
  () => {
    const styles = useStyles()
    return <div className={styles.root}>
      <div className={classNames(styles.icon, styles.iconSharp)}><AlternateEmailIcon fontSize='inherit' /></div>
      <div className={classNames(styles.icon, styles.iconBlur)}><BlurOnIcon fontSize='inherit' /></div>
    </div>
  }
