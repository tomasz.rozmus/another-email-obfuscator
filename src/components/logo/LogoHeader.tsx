import { createStyles, Theme, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import React, { FunctionComponent } from 'react'
import { fontSizeH2, fontWeightBold } from '../../theme/settings'
import { LogoImage } from './LogoImage'

const modeBreakpoint = 'md'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      cursor: 'pointer',
      display: 'flex',
      textDecoration: 'none',
      marginTop: 16,
      marginBottom: 32,
      fontSize: 6,
      [theme.breakpoints.up('sm')]: {
        fontSize: 7,
      },
      [theme.breakpoints.up(modeBreakpoint)]: {
        marginTop: 40,
        marginBottom: 65,
        fontSize: 16,
      },
    },
    text: {
      color: 'black',
      '& > *': {
        display: 'inline',
        lineHeight: 1.35
      },
      [theme.breakpoints.up(modeBreakpoint)]: {
        '& > *': {
          display: 'block',
          lineHeight: 1.1,
          fontSize: fontSizeH2,
        },
      },
    },
    first: {
      fontWeight: fontWeightBold,
    },
    second: {
      fontWeight: fontWeightBold,
    },
    third: {
      fontWeight: fontWeightBold,
      fontSize: fontSizeH2 - 12,
      color: theme.palette.primary.main,
    },
  }),
)

export const LogoHeader: FunctionComponent =
  () => {
    const styles = useStyles()

    return <span className={styles.root}>
      <LogoImage />
      <Typography variant='h1' component='span' className={styles.text}>
        <span className={styles.first}>Another </span>
        <span className={styles.second}>Email </span>
        <span className={styles.third}>Obfuscator </span>
      </Typography>
    </span>
  }
