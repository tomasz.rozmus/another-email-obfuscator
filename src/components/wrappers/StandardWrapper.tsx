import { makeStyles, Paper } from '@material-ui/core'
import classNames from 'classnames'
import React, { FunctionComponent } from 'react'
import { paperPaddingLeft, paperPaddingTop } from '../../theme/settings'

export interface IStandardWrapperProps {
  padded?: boolean
}


const useStyles = makeStyles({
  root: {},
  padded: {
    padding: `${paperPaddingTop}px ${paperPaddingLeft}px`,
  }
})

export const StandardWrapper: FunctionComponent<IStandardWrapperProps> =
  ({
     children,
     padded = true
   }) => {
    const styles = useStyles()
    const className = classNames(styles.root, {
      [styles.padded]: padded
    })
    return (
      <Paper className={className}>{children}</Paper>
    )
  }

