import { createStyles, Theme } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import React, { FunctionComponent } from 'react'
import { LogoHeader } from '../logo/LogoHeader'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      position: 'static',
      display: 'flex',
      justifyContent: 'space-between',
      alignItems: 'center'
    },
  }),
)

export const LightHeader: FunctionComponent = () => {
  const styles = useStyles()
  return <header className={styles.root}>
    <LogoHeader />
  </header>
}
