import { Box, BoxProps } from '@material-ui/core'
import React, { FunctionComponent } from 'react'

export const FormBox: FunctionComponent<BoxProps> = ({ mb, ...rest }) => {
  return <Box mb={2} {...rest} />
}
