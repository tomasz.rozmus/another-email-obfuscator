import { makeStyles } from '@material-ui/core/styles'
import classNames from 'classnames'
import React, { FunctionComponent } from 'react'

const useStyles = makeStyles({
  root: {
    '& > *': {
      margin: 4
    }
  }
})

export const SpacingBox: FunctionComponent<{ className?: string }> =
  ({
     className = '', children
   }) => {
    const styles = useStyles()
    return <div className={classNames(styles.root, className)}>{children}</div>
  }
