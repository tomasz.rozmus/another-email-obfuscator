import { AppBar, createStyles, IconButton, Theme, Toolbar, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import MenuIcon from '@material-ui/icons/Menu'
import React, { FunctionComponent } from 'react'

interface IHeaderProps {

}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
  }),
)

export const Header: FunctionComponent<IHeaderProps> = ({ children }) => {
  const styles = useStyles()
  return <AppBar position='static'>
    <Toolbar>
      <IconButton edge='start' className={styles.menuButton} color='inherit' aria-label='menu'>
        <MenuIcon />
      </IconButton>
      <Typography variant='h6' className={styles.title}>
      </Typography>
    </Toolbar>
  </AppBar>
}
