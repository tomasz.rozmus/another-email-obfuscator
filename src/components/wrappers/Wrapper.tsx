import { Container } from '@material-ui/core'
import React, { FunctionComponent } from 'react'

interface IWrapperProps {

}

export const Wrapper: FunctionComponent<IWrapperProps> = ({ children }) => <Container maxWidth='lg'>
  {children}
</Container>
