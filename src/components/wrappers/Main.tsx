import React, { FunctionComponent } from 'react'

interface IMainProps {

}

export const Main: FunctionComponent<IMainProps> = ({ children }) => <>
  {children}
</>
