import { createStyles, Theme } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import classNames from 'classnames'
import React, { FunctionComponent, HTMLProps } from 'react'
import { bigMargin, mediumMargin, smallMargin, veryBigMargin } from '../../theme/settings'

interface ISectionProps {
  gutter?: 'small' | 'medium' | 'big' | 'veryBig'
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      marginBottom: smallMargin,
    },
    gutterMedium: {
      marginBottom: mediumMargin,
    },
    gutterBig: {
      marginBottom: bigMargin,
    },
    gutterVeryBig: {
      marginBottom: veryBigMargin
    }
  }),
)

export const Section: FunctionComponent<ISectionProps & HTMLProps<any>> = (
  {
    children,
    gutter,
    ...rest
  }) => {
  const styles = useStyles()
  const className = classNames(styles.root, {
    [styles.gutterMedium] : gutter === 'small',
    [styles.gutterBig] : gutter === 'medium',
    [styles.gutterVeryBig] : gutter === 'big',
  })
  return <section className={className} {...rest}>
    {children}
  </section>
}
