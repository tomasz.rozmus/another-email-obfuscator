import { createStyles, Theme } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import classNames from 'classnames'
import React, { FunctionComponent } from 'react'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      color: theme.palette.primary.main,
      textDecoration: 'underline',
      '&:visited': {
        color: theme.palette.primary.main,
      }
    },
    hashtag: {
      textDecoration: 'none'
    },
    anchor: {
      textDecoration: 'none'
    },
  }),
)

export interface ILinkProps {
  hashtag?: boolean,
  anchor?: boolean,
  href?: string
}

export const Link: FunctionComponent<ILinkProps> =
  ({
     hashtag = false,
     anchor = false,
     children,
     ...rest
   }) => {
    const styles = useStyles()
    const className = classNames(styles.root, {
      [styles.hashtag]: hashtag
    })
    return <a className={className} {...rest}>{children}</a>
  }
