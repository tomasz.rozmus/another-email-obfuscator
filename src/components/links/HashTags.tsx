import { Typography } from '@material-ui/core'
import React, { FunctionComponent } from 'react'
import { Link } from './Link'

interface IHashTagsProps {
  source: {
    medium: string,
    tags: string[]
  },
}

export const HashTags: FunctionComponent<IHashTagsProps> = ({ source }) => {
  const { medium, tags } = source
  return (
    <>
      {tags.map((tag, index) =>
        <Typography key={tag} variant='body1' component='span'>
          <Link hashtag href={`${medium}${tag}`}>#{tag}</Link>
          {index === tags.length - 1 ? null : <span>, </span>}
        </Typography>)}
    </>
  )
}
