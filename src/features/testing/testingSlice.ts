import { createSelector, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { RootState } from '../../store/rootReducer'

type TestingState = {
  environmentFontSize: number,
}

const initialState: TestingState = {
  environmentFontSize: 16
}

const testingSlice = createSlice({
  name: 'testing',
  initialState,
  reducers: {
    setEnvironmentFontSize (state, { payload }: PayloadAction<number>) {
      state.environmentFontSize = payload
    }
  }
})

export const {
  setEnvironmentFontSize,
} = testingSlice.actions

export const testingReducer = testingSlice.reducer

export const $testing = (state: RootState) => state.testing
export const $environmentFontSize = createSelector($testing, testing => testing.environmentFontSize)
