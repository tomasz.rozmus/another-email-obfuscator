export const mergeCode = (styles: string, markup: string) => {
  return `<span id='eo-code'><style>${styles}</style><span>${markup}</span></span>`
}
