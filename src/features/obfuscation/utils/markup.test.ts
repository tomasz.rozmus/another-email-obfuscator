import { mergeCode } from './markup'

test('merges code', () => {
  const result = mergeCode('foo', 'bar')
  expect(result).toBe(`<span id='eo-code'><style>foo</style><span>bar</span></span>`)
})
