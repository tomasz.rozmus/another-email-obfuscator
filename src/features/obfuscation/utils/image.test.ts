import { getImageDataUrl } from './image'

test('generates url', () => {
  expect(getImageDataUrl('0x0001', 'png')).toEqual('data:image/png;base64,0x0001')
})
