// @ts-ignore
import getContext from 'get-canvas-context'
// @ts-ignore
import svgToImage from 'svg-to-image'

type ImageType = 'svg' | 'jpg' | 'jpeg' | 'png' | 'gif'

type GetSvgOptions =
  {
    text: string,
    width?: number,
    height?: number
  }

type GetImageOptions = GetSvgOptions &
  {
    type?: ImageType
  }

export const getSvg = (
  {
    text,
    width = 80,
    height = 100,
  }: GetSvgOptions) => {
  return `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 ${width} ${height}" width="${width}px" height="${height}px">
    <text x="50%" y="78%" font-size="${height}px" font-family="Courier, monospace" text-anchor="middle">${text}</text>
</svg>
`
}

export const getImageDataUrl = (imgData: string, fileType: string) => `data:image/${fileType};base64,${imgData}`

export const getTextImageDataUrl = ({ text, type = 'svg' }: GetImageOptions) => new Promise<string>((resolve, reject) => {
  const size = 30
  const ratio = 0.6
  const width = Math.round(ratio * size) * text.length
  const height = size

  const context = getContext('2d', {
    width, height
  })

  const svgSource = getSvg({ text, width, height })

  if (type === 'svg') {
    const svgImageData = getImageDataUrl(window.btoa(svgSource), 'svg+xml')
    resolve(svgImageData)
  }

  svgToImage(svgSource, (err: any, image: any) => {
    if (err) reject(err)

    // draw image to canvas
    context.drawImage(image, 0, 0)

    // append to DOM
    // document.body.appendChild(context.canvas)

    const dataUrl = context.canvas.toDataURL(`image/${type}`)
    resolve(dataUrl)
  })
})
