import React, { FunctionComponent, useEffect, useState } from 'react'
import { GeneralSettings, IStrategy } from '../../../configuration/configurationSlice'
import { useStrategyRender } from '../../hooks/useStrategyRender'
import { getTextImageDataUrl } from '../../utils/image'
import { makeStrategyStyles } from '../../hooks/makeStrategyStyles'

const useStyles = makeStrategyStyles(() => {
  return ({
    '@keyframes blurLoopAnimation': {
      '0%': { filter: 'blur(0.5em)' },
      '100%': { filter: 'blur(0em)' },
    },
    container: {
      fontSize: (props: RenderProps) => `${props.fontSize}`,
      animationName: 'nonexistent',
      animationDuration: '0.3s',
      animationIterationCount: 'infinite',
      animationDirection: 'alternate',
      animationTimingFunction: 'ease-out',
      animationPlayState: 'paused',
      filter: 'blur(0.5em)',
      '& > *': {
        display: 'inline-block',
      },
      '&:hover': {
        animationName: '$blurLoopAnimation',
        animationPlayState: 'running'
      }
    },
    email: {
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center center',
      backgroundSize: 'contain',
      backgroundImage: (props: RenderProps) => `url(${props.emailUrl})`,
      '&:before': {
        opacity: 0,
        content: (props: RenderProps) => `"${'x'.repeat(props.emailLength)}"`
      }
    },
  })
})

type RenderProps = GeneralSettings & {
  emailUrl: string,
  emailLength: number,
}

const getInitialRenderProps: (generalSettings: GeneralSettings) => RenderProps = (generalSettings) => ({
  ...generalSettings,
  emailUrl: '',
  emailLength: 0,
})

const Render: FunctionComponent<RenderProps> =
  (props) => {
    const styles = useStyles(props)
    return <span className={styles.container}>
    <span className={styles.email} />
  </span>
  }

export const HoverableBlurLoopStrategy: FunctionComponent<IStrategy> =
  ({
     email,
     generalSettings,
     onHtmlReady
   }) => {
    const [renderProps, setRenderProps] = useState<RenderProps>(getInitialRenderProps(generalSettings))
    const [ready, setReady] = useState(false)

    useEffect(() => {
      (async function generateDataUrl () {
        setReady(false)
        const emailUrl = await getTextImageDataUrl({ text: email })
        const emailLength = email.length
        setRenderProps({ ...generalSettings, emailUrl, emailLength })
        setReady(true)
      })()
    }, [email, generalSettings])

    useStrategyRender({ ready, renderProps, onHtmlReady, Render})

    return null
  }
