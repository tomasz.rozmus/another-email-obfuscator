import classNames from 'classnames'
import React, { FunctionComponent, useEffect, useState } from 'react'
import { GeneralSettings, IStrategy } from '../../../configuration/configurationSlice'
import { useStrategyRender } from '../../hooks/useStrategyRender'
import { getTextImageDataUrl } from '../../utils/image'
import { makeStrategyStyles } from '../../hooks/makeStrategyStyles'

const useStyles = makeStrategyStyles(() => {
  return ({
    container: {
      fontSize: (props: RenderProps) => `${props.fontSize}`,
      '& > *': {
        display: 'inline-block',
      }
    },
    shared: {
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center center',
      backgroundSize: 'contain',
      transition: '1.5s filter linear;',
      '&:hover': {
        filter: 'blur(0px)',
        transition: '0.5s filter linear;'
      },
      '&:before': {
        opacity: 0
      }
    },
    at: {
      filter: 'blur(0.1em)',
      '&:before': {
        opacity: 1,
        content: '"@"',
      }
    },
    local: {
      filter: 'blur(0.4em)',
      backgroundImage: (props: RenderProps) => `url(${props.localUrl})`,
      '&:before': {
        content: (props: RenderProps) => `"${'x'.repeat(props.localLength)}"`
      }
    },
    domain: {
      filter: 'blur(0.25em)',
      backgroundImage: (props: RenderProps) => `url(${props.domainUrl})`,
      '&:before': {
        content: (props: RenderProps) => `"${'x'.repeat(props.domainLength)}"`
      }
    }
  })
})

type RenderProps = GeneralSettings & {
  localUrl: string,
  localLength: number,
  domainUrl: string,
  domainLength: number
}

const getInitialRenderProps: (generalSettings: GeneralSettings) => RenderProps = (generalSettings) => ({
  ...generalSettings,
  localUrl: '',
  localLength: 0,
  domainUrl: '',
  domainLength: 0,
})

const Render: FunctionComponent<RenderProps> =
  (props) => {
    const styles = useStyles(props)
    return <span className={styles.container}>
    <span className={classNames(styles.shared, styles.local)} />
    <span className={classNames(styles.shared, styles.at)} />
      <span className={classNames(styles.shared, styles.domain)} />
  </span>
  }

export const HoverableBlurPartsStrategy: FunctionComponent<IStrategy> =
  ({
     email,
     generalSettings,
     onHtmlReady
   }) => {
    const [renderProps, setRenderProps] = useState<RenderProps>(getInitialRenderProps(generalSettings))
    const [ready, setReady] = useState(false)

    useEffect(() => {
      const [localPart, domainPart] = email.split('@');
      (async function generateDataUrl () {
        setReady(false)
        const localUrl = await getTextImageDataUrl({ text: localPart })
        const localLength = localPart.length
        const domainUrl = await getTextImageDataUrl({ text: domainPart })
        const domainLength = domainPart.length
        setRenderProps({ ...generalSettings, localUrl, localLength, domainUrl, domainLength })
        setReady(true)
      })()
    }, [email, generalSettings])

    useStrategyRender({ ready, renderProps, onHtmlReady, Render })

    return null
  }
