import { makeStyles } from '@material-ui/core/styles'
import { Theme as DefaultTheme } from '@material-ui/core/styles/createMuiTheme'
import { ClassNameMap, Styles } from '@material-ui/styles/withStyles'

export function makeStrategyStyles<
  Theme = DefaultTheme,
  Props extends {} = {},
  ClassKey extends string = string> (
  style: Styles<Theme, Props, ClassKey>
): (props: Props) => ClassNameMap<ClassKey> {
  return makeStyles(style, { name: 'StrategyRender' })
}
