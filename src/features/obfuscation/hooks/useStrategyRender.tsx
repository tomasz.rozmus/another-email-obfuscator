import { createGenerateClassName, ServerStyleSheets } from '@material-ui/core/styles'
import minify from 'css-simple-minifier'
import React, { FunctionComponent, useEffect } from 'react'
import ReactDOMServer from 'react-dom/server'
import { mergeCode } from '../utils/markup'

interface IStrategyRenderHookProps {
  ready: boolean,
  onHtmlReady: (html: string) => void,
  renderProps: any,
  Render: FunctionComponent<any>
}

export const useStrategyRender: (args: IStrategyRenderHookProps) => void = (
  {
    ready,
    onHtmlReady,
    renderProps,
    Render
  }) => {
  return useEffect(() => {
    if (!ready) {
      return
    }
    console.log('rndr')
    const generateClassName = createGenerateClassName({
      productionPrefix: 'eo',
    })
    const sheets = new ServerStyleSheets({
      serverGenerateClassName: generateClassName
    })
    const markup = ReactDOMServer.renderToString(sheets.collect(
      <Render {...renderProps} />
    ))
    const styles = process.env.NODE_ENV === 'production' ? minify(sheets.toString()) : sheets.toString()
    onHtmlReady(mergeCode(styles, markup))

    return () => {
      document
        .querySelectorAll('style[data-meta=StrategyRender]')
        .forEach(elem => elem.remove())
    }
  }, [ready, renderProps, onHtmlReady])
}
