import { Box, Button, Fade, Grid, Slider, Tooltip, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import FileCopyIcon from '@material-ui/icons/FileCopy'
import FormatSizeIcon from '@material-ui/icons/FormatSize'
import React, { FunctionComponent, useCallback, useEffect, useMemo, useState } from 'react'
import CopyToClipboard from 'react-copy-to-clipboard'
import { useDispatch, useSelector } from 'react-redux'
import { Debug } from '../../components/utils/Debug'
import { Section } from '../../components/wrappers/Section'
import { SpacingBox } from '../../components/wrappers/SpacingBox'
import { tooltipEntryDelay } from '../../theme/settings'
import { $email, $settings, $strategy } from '../configuration/configurationSlice'
import { $environmentFontSize, setEnvironmentFontSize } from '../testing/testingSlice'
import { HoverableBlurLoopStrategy } from './strategies/hoverableBlurLoop/HoverableBlurLoopStrategy'
import { HoverableBlurPartsStrategy } from './strategies/hoverableBlurParts/HoverableBlurPartsStrategy'

const previewId = 'previewBox'
const copyId = 'copyBox'

const useStyles = makeStyles({
  textarea: {
    width: '100%',
    height: 200
  },
  preview: {
    fontSize: '1rem'
  },
  sliderIcon: {
    marginBottom: 0,
  },
  slider: {
    display: 'inline-block',
    marginBottom: 0,
    width: 200
  },
})

export const GeneratorView: FunctionComponent = () => {
  const dispatch = useDispatch()
  const email = useSelector($email)
  const generalSettings = useSelector($settings)
  const environmentFontSize = useSelector($environmentFontSize)
  const strategy = useSelector($strategy)
  const [html, setHtml] = useState('')
  const [copied, setCopied] = useState(false)
  const styles = useStyles()

  const handleHtmlReady = useCallback((generatedHtml: string) => {
    setHtml(generatedHtml)
  }, [])

  const handleEnvironmentFontSizeChange = useCallback((_, value) => {
    dispatch(setEnvironmentFontSize(value))
  }, [dispatch])

  useEffect(() => {
    if (!html) {
      return
    }
    const preview = document.getElementById(previewId)
    if (preview !== null) {
      preview.innerHTML = html
    }
  }, [html])

  const handleCopy = useCallback(() => {
    setCopied(true)
    setTimeout(() => setCopied(false), 1000)
  }, [])

  const ResolvedStrategy = useMemo(() => {
    switch (strategy) {
      case 'hoverableBlurLoop':
        return HoverableBlurLoopStrategy
      case 'hoverableBlurParts':
        return HoverableBlurPartsStrategy
      default:
        return HoverableBlurPartsStrategy
    }
  }, [strategy])

  if (!email) {
    return null
  }
  return (<Section>
    <Grid container spacing={2}>
      <Grid item xs={12} md={6}>
        <Box display='flex' justifyContent='space-between'>
          <Typography variant='h2' gutterBottom>Code</Typography>
          <SpacingBox>
            <Fade in={copied} timeout={{ exit: 5000 }}>
              <Typography component='span'>copied to
                clipboard!</Typography>
            </Fade>
            <CopyToClipboard onCopy={handleCopy} text={html}>
              <Tooltip title='Click to copy' arrow placement='top' enterDelay={tooltipEntryDelay}>
                <Button size='small' color='primary' startIcon={<FileCopyIcon />}>Copy</Button>
              </Tooltip>
            </CopyToClipboard>
          </SpacingBox>
        </Box>
        <textarea className={styles.textarea} id={copyId} value={html} disabled />
      </Grid>
      <Grid item xs={12} md={6}>
        <Box display='flex' justifyContent='space-between'>
          <Typography variant='h2' gutterBottom>Preview</Typography>
          <SpacingBox>
            <Tooltip title='Slide to adjust font size' arrow placement='top' enterDelay={tooltipEntryDelay}>
              <FormatSizeIcon color='primary' className={styles.sliderIcon} />
            </Tooltip>
            <div className={styles.slider}>
              <Slider
                value={environmentFontSize}
                aria-labelledby='discrete-slider-custom'
                min={4} step={1} max={48}
                valueLabelDisplay='auto'
                onChange={handleEnvironmentFontSizeChange}
                valueLabelFormat={v => `${v}px`}
              />
            </div>
          </SpacingBox>
        </Box>
        <div className={styles.preview} id={previewId} style={{ fontSize: `${environmentFontSize}px` }} />
      </Grid>
    </Grid>
    <ResolvedStrategy
      onHtmlReady={handleHtmlReady}
      email={email}
      generalSettings={generalSettings}
    />
    < Debug
      target={
        { email, length: html.length }
      }
    />
  </Section>)
}
