import {
  Button,
  Collapse,
  FormControl,
  FormControlLabel,
  Grid,
  InputLabel,
  MenuItem,
  Select,
  Switch,
  Typography
} from '@material-ui/core'
import { Field, Form, Formik } from 'formik'
import { TextField } from 'formik-material-ui'
import React, { FunctionComponent, useCallback, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { FormBox } from '../../components/wrappers/FormBox'
import { Section } from '../../components/wrappers/Section'
import { StandardPaper } from '../../components/wrappers/StandardPaper'
import { configurationSchema } from './configurationSchema'
import { $strategy, setEmail, setFontSize, setStrategy } from './configurationSlice'

const initialValues = { email: '', fontSize: '' }

export const ConfigurationView: FunctionComponent = () => {
  const dispatch = useDispatch()
  const strategy = useSelector($strategy)
  const [advanced, setAdvanced] = useState(false)

  const handleStrategyChange = useCallback((event) => {
    dispatch(setStrategy(event.target.value))
  }, [dispatch])

  const toggleAdvancedSettings = useCallback(() => {
    setAdvanced(!advanced)
  }, [advanced])

  const handleSubmit = useCallback((values, { setSubmitting }) => {
    const { email, fontSize } = values
    setTimeout(() => {
      dispatch(setEmail(email))
      dispatch(setFontSize(fontSize))
      setSubmitting(false)
    }, 500)
  }, [dispatch])

  return <Section>
    <Formik initialValues={initialValues} onSubmit={handleSubmit} validationSchema={configurationSchema}
            validateOnBlur={true}
    >
      {({ submitForm, isSubmitting }) => (
        <Form>
          <Grid container spacing={3}>
            <Grid item xs={12} md={6}>
              <Typography variant='h2' gutterBottom>General settings</Typography>
              <StandardPaper>
                <FormBox>
                  <Field component={TextField}
                         name='email'
                         label='Email'
                         type='email' helperText='email address to obfuscate' />
                </FormBox>
                <FormBox display='flex' justifyContent='flex-end'>
                  <FormControlLabel
                    control={<Switch
                      checked={advanced}
                      onChange={toggleAdvancedSettings}
                      color='primary'
                    />}
                    label={<Typography variant='body2'>more settings</Typography>}
                    labelPlacement='start'
                  />
                </FormBox>
                <Collapse in={advanced}>
                  <FormBox>
                    <Field component={TextField}
                           name='fontSize'
                           label='Font size'
                           placeholder='inherit'
                           helperText='font size of generated email, ex. 1rem, 2cm, 70%, 10px...'
                    />
                  </FormBox>
                </Collapse>
              </StandardPaper>
            </Grid>
            <Grid item xs={12} md={6}>
              <Collapse in={advanced}>
                <Typography variant='h2' gutterBottom>Obfuscation</Typography>
                <StandardPaper>
                  <FormBox>
                    <FormControl fullWidth disabled={isSubmitting}>
                      <InputLabel id='strategy-label'>Strategy</InputLabel>
                      <Select
                        labelId='strategy-label'
                        value={strategy}
                        onChange={handleStrategyChange}
                      >
                        <MenuItem value='hoverableBlurParts'>Hoverable Parts</MenuItem>
                        <MenuItem value='hoverableBlurLoop'>Blur Loop (beta)</MenuItem>
                      </Select>
                    </FormControl>
                  </FormBox>
                </StandardPaper>
              </Collapse>
            </Grid>
          </Grid>
          <Grid container spacing={3}>
            <Grid item xs={12} container justify='center' alignContent='center'>
              <Button onClick={submitForm} type='submit'
                      variant='contained' color='primary'
                      size='large'>Generate</Button>
            </Grid>
          </Grid>
        </Form>
      )}
    </Formik>
  </Section>
}
