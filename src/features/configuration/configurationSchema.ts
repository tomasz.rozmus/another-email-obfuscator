import { object, string } from 'yup'

const FS_LEN_REGEX = /^\d*(\.?)\d*(px|%|em|rem|cm)$/
const FS_NAME_REGEX = /^(inherit|initial)$/
const FS_REGEX = new RegExp(
  [FS_LEN_REGEX, FS_NAME_REGEX].map(({ source }) => source).join('|')
)

export const configurationSchema = object().shape({
  email: string()
    .email()
    .required(),
  fontSize: string()
    .matches(FS_REGEX, 'enter valid font size or leave empty')
})
