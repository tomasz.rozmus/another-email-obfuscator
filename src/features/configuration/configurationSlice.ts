import { createSelector, createSlice, PayloadAction } from '@reduxjs/toolkit'
import { RootState } from '../../store/rootReducer'
import { NullableString } from '../../types'

export interface IStrategy {
  email: string,
  onHtmlReady: (html: string) => void,
  generalSettings: GeneralSettings
}

export type Strategy = 'hoverableBlurParts' | 'hoverableBlurLoop'

export type GeneralSettings = {
  fontSize: string,
  blur?: string | number
}

type ConfigurationState = {
  email: NullableString,
  strategy: Strategy,
  settings: GeneralSettings
}

const initialState: ConfigurationState = {
  email: null,
  strategy: 'hoverableBlurParts',
  settings: {
    fontSize: 'inherit'
  }
}

const configurationSlice = createSlice({
  name: 'configuration',
  initialState,
  reducers: {
    setEmail (state, { payload }: PayloadAction<string>) {
      state.email = payload
    },
    setStrategy (state, { payload }: PayloadAction<Strategy>) {
      state.strategy = payload
    },
    setFontSize (state, { payload }: PayloadAction<string>) {
      state.settings.fontSize = payload ? payload : 'inherit'
    }
  }
})

export const {
  setEmail,
  setFontSize,
  setStrategy
} = configurationSlice.actions

export const configurationReducer = configurationSlice.reducer

export const $configuration = (state: RootState) => state.configuration
export const $email = createSelector($configuration, configuration => configuration.email)
export const $strategy = createSelector($configuration, configuration => configuration.strategy)
export const $settings = createSelector($configuration, configuration => configuration.settings)
