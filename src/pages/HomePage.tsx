import { Typography } from '@material-ui/core'
import React, { FunctionComponent } from 'react'
import { Section } from '../components/wrappers/Section'
import { ConfigurationView } from '../features/configuration/ConfigurationView'
import { GeneratorView } from '../features/obfuscation/GeneratorView'

export const HomePage: FunctionComponent = () => <>
  <Typography variant='h1' gutterBottom component='h1'>Email obfuscator</Typography>
  <Section>
    <Typography variant='subtitle2' component='p' gutterBottom>This is another email obfuscator. It combines in-browser
      generated images with some CSS tricks to mislead OCR-based email crawlers. Find out more <a href='#more'>here.</a></Typography>
  </Section>
  <ConfigurationView />
  <GeneratorView />
  <Section id='more'>
    <Typography variant='h2' gutterBottom>How it works?</Typography>
    <Typography variant='subtitle2' component='p'>
      Enter email, click generate and copy-paste the code in your website.
    </Typography>
    <Typography variant='subtitle2' component='p'>
      It can be read only after <strong>user interaction with generated element</strong> (hovering or tapping).
    </Typography>
    <Typography variant='body1' component='div' gutterBottom>
      <p>If you are curious, here are some technicalities:</p>
      <ul>
        <li>Generation process occurs in browser</li>
        <li>No data is being sent anywhere, you can check this in developer tools</li>
        <li>There is no email encoded in generated CSS. No chance being indexed by text-based crawlers</li>
        <li>Generated code works without JS enabled in the browser</li>
        <li>Generated code (in basic mode) will dynamically adapt to your website font-size settings</li>
      </ul>
    </Typography>
  </Section>
</>
