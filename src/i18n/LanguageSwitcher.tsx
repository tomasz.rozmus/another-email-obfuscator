import { Button } from '@material-ui/core'
import LanguageIcon from '@material-ui/icons/Language'
import React, { FunctionComponent } from 'react'
import { useTranslation } from 'react-i18next'
export const LanguageSwitcher: FunctionComponent = () => {
  const { i18n } = useTranslation()
  const changeLanguage = (lng: string) => {
    i18n.changeLanguage(lng).then()
  }
  return <>
    <Button onClick={() => changeLanguage('ch')}>&nbsp;</Button>
    <LanguageIcon />
    <Button onClick={() => changeLanguage('pl')}>Polski</Button>
    <Button onClick={() => changeLanguage('en')}>English</Button>
  </>
}
