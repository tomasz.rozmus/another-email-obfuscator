import { CssBaseline, ThemeProvider } from '@material-ui/core'
import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import './index.css' // TODO: inject into .html
import * as serviceWorker from './serviceWorker'
import { store } from './store/store'
import { theme } from './theme/theme'

const render = () => {
  const App = require('./App').default

  const rootElement = document.getElementById('root')
  const Application = () => <Provider store={store}>
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <App />
    </ThemeProvider>
  </Provider>

  if (rootElement?.hasChildNodes()) {
    ReactDOM.hydrate(<Application />, rootElement)
  } else {
    ReactDOM.render(<Application />, rootElement)
  }

}

render()

// @ts-ignore
if (process.env.NODE_ENV === 'development' && module.hot) {
  // @ts-ignore
  module.hot.accept('./App', render)
}
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
