#!/bin/sh

# Recreate config file
rm -rf ./env-config.js
touch ./env-config.js

# Add assignment
echo "window._env_ = {" >> ./env-config.js

# Read each line in .env file
# Each line represents key=value pairs
while read -r line || [ -n "$line" ];
do
  [ "${line%${line#?}}"x = '#x' ] && continue

  # Split env variables by character `=`
  if printf '%s\n' "$line" | grep -q -e '='; then
    varname=$(printf '%s\n' "$line" | sed -e 's/=.*//')
    varvalue=$(printf '%s\n' "$line" | sed -e 's/^[^=]*=//')
  fi

  # Read value of current variable if exists as Environment variable
  # echo "name: ${varname}"
  # echo "file: ${varvalue}"
  hostvalue=$(printenv $varname)
  # echo "host: ${hostvalue}"

  # Otherwise use value from .env file
  if [ -z $hostvalue ]; then
    # echo "value from .env"
    value=${varvalue}
  else
    # echo "value from hostvalue"
    value=${hostvalue}
  fi

  line="  $varname: \"$value\","
  # Display resolved configuration:
  # echo $line
  # Append configuration property to JS file
  echo $line >> ./env-config.js
done < .env

echo "}" >> ./env-config.js